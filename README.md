## Tübitak 1001 Projesi Sonuç Raporu Latex Şablonu

[Can Alkan'ın şablonunun](https://tr.overleaf.com/latex/templates/tubitak-sonuc-raporu-sablonu/gtcspgqbnqcx) PSR_KURALLAR.pdf'e göre güncellenmiş halidir.

TÜBİTAK logosunun telif hakkı TÜBİTAK'a aittir.

Sonuç raporunun Arial fontunda yazımı istenmektedir. Bu font Linux'te bulunmuyor. Arch Linux tabanlı işletim sistemlerinde AUR üzerinden ```urw-arial``` paketi ile yüklenebilir.

MacOS ve Windowsta hata verebilir. Bu durumda,
```latex
\usepackage{uarial}
\renewcommand{\sfdefault}{ua1}  
\renewcommand{\familydefault}{\sfdefault}
```
satırlarını silip, kendiniz Arial fontu eklemeye çalışabilirsiniz.